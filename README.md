RD Training Project.
Built Online Quiz Application with Spring Boot Framework which supports various interfaces like Web and Console.

Developed Online Quiz Application in which Admin can create Quiz and User can take Quiz on web browser with JSP pages User Interfaces.

Integrated the Spring MVC Application with MySQL Database with Spring Data JPA.
Secured the application with Spring Security and also developed Spring RESTful Web Services through Swagger-UI.
Achieved the 90+ % Test coverage for Controller and Service layer.

Database	MySQL
Tools	Eclipse, MySQL Workbench, Postman, Chrome Browser.

Please shift to master branch to view source code.